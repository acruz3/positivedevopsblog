import Index from '../index';
import React from 'react';
import { mount } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });


test('Index: text for the hyperlink is correct', () => {
  const wrapper = mount(
    <Index />
  );
  const hyperlink = wrapper.find('#first_lesson');
  expect(hyperlink.text()).toBe('My brief resume of this lesson');
});
